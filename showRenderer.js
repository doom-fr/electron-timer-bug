const {ipcRenderer} = require('electron');

const showExitBtn = document.getElementById('show-exit-button');

const DelayRendererWatchdog=1000;
let rendererWatchdog=setTimeout(()=>console.log('renderer callback'),DelayRendererWatchdog);
console.log('rendererWatchdog : ',rendererWatchdog);
console.log('typeof(rendererWatchdog) : ',typeof rendererWatchdog);
if(typeof rendererWatchdog === 'object')
  console.log('rendererWatchdog.constructor.name: ',rendererWatchdog.constructor.name);


showExitBtn.addEventListener('click', function () {
    ipcRenderer.send('show-exit');
});
