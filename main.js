const electron = require('electron');
const { app, BrowserWindow, ipcMain } = electron;
const path = require('path');



const ShowHtml = path.join('file://', __dirname, 'show.html');



const onAppReady = function () {
    let showWindow = new BrowserWindow({
      webPreferences: { nodeIntegration: true },
      width:1200,height:800
    });
    showWindow.once('close', () => {
      console.log('close the show window');
      showWindow = null;
    });
    showWindow.loadURL(ShowHtml);
    showWindow.webContents.openDevTools();
    
    const DelayMainWatchdog=1000;
    let mainWatchdog=setTimeout(()=>console.log('main callback'),DelayMainWatchdog);
    console.log('mainWatchdog : ',mainWatchdog);
    console.log('typeof(mainWatchdog) : ',typeof mainWatchdog);
    if(typeof mainWatchdog === 'object')
      console.log('mainWatchdog.constructor.name: ',mainWatchdog.constructor.name);

  ipcMain.on('show-exit', (/*event, arg*/) => {
    console.log('show-exit event');
    showWindow.close();
    app.quit();
  });

};


app.on('ready', () => setTimeout(onAppReady, 500));
